

IF EXIST "%cd%\download\*.jar" (move /Y %cd%\download\*.jar %cd%\target)

dir /b/s *.jar > pathRun.txt

SET /p APPLICATION=<pathRun.txt
SET MODULE=--add-modules=javafx.base,javafx.controls,javafx.fxml,javafx.graphics,javafx.media,javafx.swing,javafx.web

java -Dfile.encoding=UTF-8 --module-path "%PATH_TO_FX%"  %MODULE% -jar %APPLICATION%