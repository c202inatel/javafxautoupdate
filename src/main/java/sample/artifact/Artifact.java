package sample.artifact;


import java.util.List;

public class Artifact {
  //  @JsonProperty("values")
    private List<PropertyArtifact> values;

    public List<PropertyArtifact> getValues() {
        return values;
    }

    public void setValues(List<PropertyArtifact> values) {
        this.values = values;
    }
}
