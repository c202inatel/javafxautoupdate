package sample.artifact;

import java.util.Date;

public class PropertyArtifact {

    private long size;

    private Date created_on;

    //@JsonProperty("name")
    private String name; //links

    // @JsonProperty("links")
    private Links links;

    public long getSize() {
        return size;
    }

    public void setSize(long size) {
        this.size = size;
    }

    public Date getCreatedOn() {
        return created_on;
    }

    public void setCreatedOn(Date createdOn) {
        this.created_on = createdOn;
    }

    public String getNameFile() {
        return name;
    }

    public void setNameFile(String nameFile) {
        this.name = nameFile;
    }

    public Links getLinks() {
        return links;
    }

    public void setLinks(Links links) {
        this.links = links;
    }
}
