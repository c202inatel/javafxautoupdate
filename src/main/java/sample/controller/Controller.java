package sample.controller;

import javafx.application.Platform;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.ProgressBar;
import sample.util.FindArtifactInCloudRepository;

import java.io.IOException;
import java.net.URL;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.ResourceBundle;
import java.util.concurrent.Executors;
import java.util.logging.Logger;

public class Controller implements Initializable {
    private final Logger logger = Logger.getLogger(getClass().getSimpleName());

    @FXML
    private ProgressBar progress;

    @FXML
    private Button button;

    @FXML
    private Label message;

    @Override
    public void initialize(URL url, ResourceBundle resourceBundle) {
        progress.setVisible(false);
    }

    @FXML
    void onDownloadClick(ActionEvent event) {
        FindArtifactInCloudRepository repository = new FindArtifactInCloudRepository();
        repository.findArtifact().ifPresent(property -> {
            if (property.getNameFile().endsWith(".jar")) {
                final var url = property.getLinks().getSelf().getHref();
                final var destiny = "download/" + property.getNameFile();

                if (ifNotExistFileWithSameName(System.getProperty("user.dir"), property.getNameFile())) {
                    final DownloadArtifacts task = new DownloadArtifacts(url, destiny, property.getSize());
                    task.setOnFailed(work -> Platform.runLater(() -> {
                        var ex = work.getSource().getException();
                        logger.warning(ex.getClass().getSimpleName() + ":" + ex.getMessage());
                        task.deleteArtifact();
                    }));
                    Executors.newWorkStealingPool().submit(task);

                    button.textProperty().bind(task.messageProperty());
                    button.disableProperty().bind(task.runningProperty());
                    progress.visibleProperty().bind(task.runningProperty());
                    progress.progressProperty().bind(task.progressProperty());


                } else {
                    System.out.println("Did you do download the last version");
                }
            }
        });
    }

    private boolean ifNotExistFileWithSameName(String path, String nameFile) {
        try (var getOnlyFile = Files.walk(Paths.get(path)).filter(Files::isRegularFile)) {
            var foundFileWithName = getOnlyFile.filter(paths -> paths.toFile().getName().equals(nameFile));
            return foundFileWithName.findFirst().isEmpty();
        } catch (Exception ex) {
            System.out.println(ex.getMessage());
        }
        return true;
    }


}
