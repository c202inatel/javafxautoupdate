package sample.controller;


import javafx.concurrent.Task;
import sample.util.Authorization;

import java.io.BufferedInputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.net.HttpURLConnection;
import java.net.URL;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.nio.file.StandardCopyOption;
import java.util.logging.Logger;

public class DownloadArtifacts extends Task<Integer> {
    private final Logger logger = Logger.getLogger(getClass().getSimpleName());
    private final String url;
    private final String pathDestiny;
    private final long sizeFile;

    public DownloadArtifacts(String url, String pathDestiny, long sizeFile) {
        this.url = url;
        this.pathDestiny = pathDestiny;
        this.sizeFile = sizeFile;
    }

    @Override
    protected Integer call() throws Exception {
        updateMessage("Aguarde finalização do processo");

        BufferedInputStream bis = new BufferedInputStream(connectionHttp(url).getInputStream());
        FileOutputStream fis = new FileOutputStream(pathDestiny);
        byte[] buffer = new byte[(int) sizeFile];
        int count = 0;
        while ((count = bis.read(buffer, 0, 1024)) != -1) {
            fis.write(buffer, 0, count);
            updateProgress(progressDownload(fis.getChannel().size(), sizeFile), 100);
        }

        bis.close();
        fis.close();

        return 100;
    }


    @Override
    protected void done() {
        updateMessage("Download");
        var root = System.getProperty("user.dir");
        File file = new File(root.concat("\\").concat(pathDestiny));
        final var moveTo = root + "\\target\\" + file.getName();

        try {
            Files.move(file.toPath(), Path.of(moveTo), StandardCopyOption.REPLACE_EXISTING);
        } catch (Exception ex) {
            System.out.println(ex.getMessage());
        }
    }

    private int progressDownload(long value, long max) {
        return (int) ((value * 100) / max);
    }

    public HttpURLConnection connectionHttp(String url) throws IOException {
        var connection = (HttpURLConnection) (new URL(url).openConnection());
        connection.setRequestProperty(Authorization.HEADER, Authorization.PASSWORD_BASE_64);
        return connection;
    }

    public void deleteArtifact() {
        try {
            Files.deleteIfExists(Paths.get(pathDestiny));
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

}
