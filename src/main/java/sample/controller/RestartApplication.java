package sample.controller;

import sample.Main;

import java.io.BufferedReader;
import java.io.File;
import java.io.InputStreamReader;
import java.util.concurrent.Executors;
import java.util.logging.Level;
import java.util.logging.Logger;

public class RestartApplication {
    public static void restartApplication(String appName) {


        Executors.newWorkStealingPool().submit(() -> {
//            String path = InfoTool.getBasePathForClass(Main.class);
            String[] applicationPath = {new File(appName).getAbsolutePath()};
            System.out.println("RUNNER FILE: " + appName);

            try {

                File applicationFile = new File(applicationPath[0]);
                while (!applicationFile.exists()) {
                    Thread.sleep(50);
                    System.out.println("Waiting " + appName + " Jar to be created...");
                }

                System.out.println(appName + " Path is : " + applicationPath[0]);

                //Create a process builder
                ProcessBuilder builder = new ProcessBuilder("java", "-jar", applicationPath[0], appName);
                builder.redirectErrorStream(true);
                Process process = builder.start();
                BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(process.getInputStream()));


                // Continuously Read Output to check if the main application started
                String line;
                while (process.isAlive())
                    while ((line = bufferedReader.readLine()) != null) {
                        if (line.isEmpty())
                            break;
                            //This line is being printed when XR3Player Starts
                            //So the AutoUpdater knows that it must exit
                        else if (line.contains("XR3Player ready to rock!"))
                            System.exit(0);
                    }

            } catch (Exception ex) {
                Logger.getLogger(Main.class.getName()).log(Level.INFO, null, ex);


            }
        });
    }
}
