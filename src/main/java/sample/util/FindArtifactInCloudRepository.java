package sample.util;

import com.google.gson.Gson;
import kong.unirest.HttpResponse;
import kong.unirest.JsonNode;
import kong.unirest.Unirest;
import sample.artifact.Artifact;
import sample.artifact.PropertyArtifact;

import java.util.Comparator;
import java.util.Optional;
import java.util.logging.Logger;
import java.util.stream.Stream;

public class FindArtifactInCloudRepository {
    private static Logger log = Logger.getLogger(FindArtifactInCloudRepository.class.getSimpleName());
    private static final String URL = "https://api.bitbucket.org/2.0/repositories/c202inatel/javafxautoupdate/downloads";

    public Optional<PropertyArtifact> findArtifact() {
        HttpResponse<JsonNode> response = Unirest.get(URL)
                .basicAuth(Authorization.USER, Authorization.PASSWORD)
                .asJson();

        Artifact artifact = new Gson().fromJson(response.getBody().toString(), Artifact.class);

        Stream<PropertyArtifact> artifactsStream = artifact.getValues().stream().sorted(sortedDateCreateOnDes());
        return artifactsStream.findFirst();
    }


    private static Comparator<PropertyArtifact> sortedDateCreateOnDes() {
        return ((o1, o2) -> o2.getCreatedOn().compareTo(o1.getCreatedOn()));
    }


}
